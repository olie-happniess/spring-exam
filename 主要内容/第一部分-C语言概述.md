




# 第一部分 C 语言概述

<img src="Image/image-20220313225914350.png" alt="image-20220313225914350" style="zoom: 67%;" />

## 1 进入C语言的世界



### 1.1 C 语言用来创建空间小、速度快的程序

C语言旨在创建空间小、速度快的程序。它比其他大多数语言的抽象层次更低，也就是说用C语言写的代码更加接近机器语言。



### 1.2 C 语言的工作方式

计算机只理解一种语言——机器代码，即一串二进制 0、1流。你可以在编译器的帮助下将C代码转化为机器代码。

<img src="Image/image-20220313230231662.png" alt="image-20220313230231662" style="zoom:67%;" />

### 1.3 C 语言编写、编译、运行实战



#### （1）编写代码，文件以 .c 结尾。

```c

#include<stdio.h>
int main(){
    printf("Hello C.");
    getchar();
    return 0;
}

```



#### （2）编译、运行代码

C 语言是一种编译型语言，也就是说计算机不会直接解释代码，而是需要将给人阅读的源代码转化（或编译）为机器能够理解的机器代码，这样计算机才能够执行。

为了编译代码，需要一个叫编译器的程序。<span style="color:red;font-weight:bold;">GNU编译器套件（GNU Compiler Collection），也叫 gcc，是最流行的C编译器之一 </span>。gcc 可以在很多操作系统中使用，而且除了C语言，它还可以编译很多其他语言，最重要的是，它是完全免费的。



##### 安装 GCC

GCC 官网提供的 GCC 编译器是无法直接安装到 Windows 平台上的，如果我们想在 Windows 平台使用 GCC 编译器，可以安装 GCC 的移植版本。目前适用于 Windows 平台、受欢迎的 GCC 移植版主要有 2 种，分别为 MinGW 和 Cygwin。其中，MinGW 侧重于服务 Windows 用户可以使用 GCC 编译环境，直接生成可运行 Windows 平台上的可执行程序，相比后者体积更小，使用更方便；



##### 安装 MinGW

（1）打开 [MinGW 官网](https://osdn.net/projects/mingw/)（点击即可进入官网），下载 MinGW 安装包。

<img src="Image/2-200G0155032123.gif" alt="官网下载MinGW" style="zoom:80%;" />



（2） 下载完成后，会得到一个名为 mingw-get-setup.exe 的安装包，双击打开它，可以看到如下的对话框：

<img src="Image/image-20220313232429815.png" alt="image-20220313232429815" style="zoom: 67%;" />



（3）直接点击“Install”，进入下面的对话框：

![自定义MinGW的安装位置](Image/2-200G0155430540.gif)



（4）根据自己操作系统的实际情况，自定义 MinGW 的安装位置，然后点击“continue”，进入下面的对话框：

![下载并安装MinGW配置器](Image/2-200G0155521960.gif)



（5）进入安装 MinGW 配置器的界面，耐心等待安装完成（显示 100%）即可。安装完成之后，我们会得到一个名为 "MinGW Installer Manager" 的软件，借助它，我们可以随时根据需要修改 GCC 编译器的配置。点击“continue”,会自动弹出配置界面，如下所示：

![配置GCC编译器](Image/2-200G016005D11.gif)



为使 GCC 同时支持编译 C 语言和 C++，需勾选图中标注的 2 项。我们知道，GCC 还支持其它编程语言，读者可借助此配置器，随时实际需要安装自己需要的编译环境。勾选完成后，在菜单栏中选择`Installation -> Apply Changes`，弹出如下对话框：

![开始安装选中的编译环境](Image/2-200G0160220954.gif)

选择“Apply”。然后耐心等待，直至安装成功，即可关闭此界面。



（6）在安装完成的基础上，我们需要手动配置 PATH 环境变量。依次`右击计算机（我的电脑） -> 属性 -> 高级系统设置 -> 环境变量`，建议读者在当前用户的 PATH 环境变量中增加 MinGW 的安装路径，例如将其安装到了`E:\MinGW`文件夹中，因此 PATH 环境变量的设置如下：

![配置PATH环境变量](Image/2-200G016041W06.gif)



（7）由此，打开命令行窗口（通过在搜索栏中执行 cmd 指令即可），输入`gcc -v`指令，如果输出 GCC 编译器的具体信息，则表示安装成功，例如：



<img src="Image/image-20220313234519335.png" alt="image-20220313234519335" style="zoom:57%;" />



（8）编译 C 语言代码

```c

/*
	这里以运行一个 C 语言程序为例（存储路径为：D:\hello.c）：
*/
#include<stdio.h>
int main(){
    printf("Hello C.");
    getchar();
    return 0;
}

```



在此基础上，在命令行窗口中执行如下指令：

```she

>gcc D:\hello.c -o D:\hello.exe

```



（9）运行程序

会在 D 盘生成一个 hello.exe 可执行文件，找到该文件并双击，即可看到程序的执行结果:

```
Hello C.
```





#### （3）使用 Hex Editor 查看 hello.exe 程序文件

![image-20220313235859150](Image/image-20220313235859150.png)





## 2 第一个程序



### 2.1 C 语言的标准

ANSI C：1989年由ANSI发布的第一个C标准

C89：1990年ISO接受了ANSI的标准，发布的C标准。 ISO C与ANSI C内容基本相同，主要是格式组织不同

C99-ISO/IEC9899：1999，作了多处改动或确认，所有 广泛使用的编译器都支持C99 

C11-ISO/IEC9899：2011，ISO发布的最新C标准。新标 准提高了对C++的兼容性，并增加了新的特性。



### 2.2 C 语言集成开发环境

集成开发环境（IDE，Integrated Development Environment ）是用于提供程序开发环境的应用程序，一般包括<span style="color:red;font-weight:bold;"> 代码编辑器、编译器、调试器和图形用户界面等工具 </span>。集成了代码编写功能、分析功能、编译功能、调试功能等一体化的开发软件服务套。所有具备这一特性的软件或者软件套（组）都可以叫集成开发环境。如微软的 Visual Studio 系列，Borland的 C++ Builder、Delphi 系列等。该程序可以独立运行，也可以和其它程序并用。IDE多被用于开发HTML应用软件。例如，许多人在设计网站时使用IDE（如HomeSite、DreamWeaver等），因为很多项任务会自动生成。



<img src="Image/image-20220318122622427.png" alt="image-20220318122622427" style="zoom:76%;border:solid 1px silver" />



### 2.3 第一个 C 语言程序

接下来，我们使用 Embarcadero Dev-C++ 6.0 集成开发环境，编写第一个 C 语言代码，代码如下所示：



（1）Hello Word 源代码

------

```
#include<stdio.h>
int main(){
	printf("Hello World");
	return 0;
}
```



（2）编写/编译/运行过程

------

![wwdfd](Image/wwdfd343d.gif)



<span style="color:green;font-weight:bold;">☆☆☆ （1） 注意：请将输入法切换到英文状态。</span>

<span style="color:green;font-weight:bold;">☆☆☆ （2） 注意：编译、运行之前先保存文件，文件未保存之前，左上角文件名旁边会出现一个 * 字符。</span>

<span style="color:green;font-weight:bold;">☆☆☆ （3）文件保存时，将文件保存为 C Source 文件类型。</span>





（3）程序框架

------

```c
/*
 * 作者：你的名字
 * 功能：程序框架
 */
#include<stdio.h>
int main(){
	return 0;
}

```

<span style="color:green;font-weight:bold;">☆☆☆ （1） 接下来，大多数的代码都会用到这个程序框架，直到我们学习函数之后。</span>







# 第二部分 C 语言基础



## 1 常量与变量

在程序的指导下，计算机可以做许多事情，如数值计算、名字排序、执 行语言或视频命令、计算彗星轨道、准备邮件列表、拨电话号码、画画、做决策或其他你能想到的事情。要完成这些任务，程序需要使用数据，即承载信息的数字和字符。有些数据类型在程序使用之前已经预先设定好了，在整个程序的运行过程中没有变化，这些称为常量（constant）。其他数据类型 在程序运行期间可能会改变或被赋值，这些称为变量（variable）。 



已知圆的面积公式，如下所示：
$$
S=πr^2
$$

> 🐷：请帮我计算一个给定半径的圆的面积，谢谢。
> 🐮：请告诉我这个圆的半径 r 是多少？（换做是你，你会怎么提问呢？）
> 🐷：半径是 5cm，你为什么不问一问 π 是多少呢？
> 🐮：半径是 5cm，那么面积是 78.5 cm<sup>2</sup>，因为 π 是一个常量，大家都知道等于 3.14，为什么还要问呢？
> 🐷：厉害，那麻烦你在帮我计算一下半径是 6cm 的圆的面积吧。
> 🐮：113.04cm<sup>2</sup>。
> 🐷：怎么这么快就计算出了呢？
> 🐮：因为我会 C 语言，代码如下，你可以看看。



```c

/*
 * 计算指定半径的圆的面积。
 */

#include<stdio.h>
#define PI 3.14

int main(){
	int r = 0;
	double s = 0.0;
	printf("请输入圆的半径 r (单位是 cm):");
	scanf("%d",&r);
	s = PI*r*r;
	printf("面积=%lf",s);
}

```



### 1.1 变量

------

如果用程序计算不同半径圆的面积，我们需要考虑以下几个问题。

> 如何告诉计算机需要计算的圆的半径是多少？
>
> 计算机用什么来保存用户告知的半径数据？



**变量是什么：**变量是在程序执行期间其值可以<span style="color:red;font-weight:bold;"> 改变的量</span>，必须先<span style="color:red;font-weight:bold;"> 定义 </span>后使用。

**变量的定义：**<span style="color:red;font-weight:bold;">类型说明符</span> <span style="color:green;font-weight:bold;">变量名1,变量名2，变量名3；</span>

```c

变量定义的格式
类型说明符 变量名1,变量名2,变量名3,…,变量名n;

int a,b,c; //可以一次定义多个变量
float x;   //也可以一次只定义一个变量

```



**变量的命名：**如何给变量起一个恰当的名字，首先必须符合 C 语言变量命名的要求，其次名字要有意义，见名知意。

规则：变量名必须是一个<span style="color:red;font-weight:bold;"> 有效标识符</span>。



**标识符**：为程序的构成成份命名，如变量名、函数名、文件名、类型名等，标识 的命名规则：

（1）**只能由字母、数字和下划线组成（**2）**首字符必须为字母或下划线**（3）**见名知意，区分大小写**（4）**不能使用系统的保留字**

```C

试一试：哪些是合法的标识符。
a,x,x3,BOOK_1,sum5,3s,s*T,-3x,bowy-1

```



**保留字**：C语言预先规定的，具有特定意义的字母组合，保留给语言本身使用，也称为<span style="color:red;font-weight:bold;"> 关键字</span>。

<img src="Image/image-20220327123235814.png" alt="image-20220327123235814" style="zoom:70%;" />



**变量的赋值：**变量定义之后，若不对其进行赋值，那么变量的意义就小了很多。

在 C 语言中，= 并不意味着 “相等”，而是一个赋值运算符。下面的<span style="color:red;font-weight:bold;"> 赋值 </span>表达式语句： 

```c

bmw = 2002; 

```

把值 2002 赋给变量 bmw。也就是说，= 号左侧是一个变量名，右侧是赋给该变量的值。符号 = 被称为赋值运算符。另外，上面的语句不读作 “bmw等 于2002”，而读作 “把值2002赋给变量bmw”。赋值行为从右往左进行。 

 

**变量的赋值过程**：通过以下代码，我们来看一下 C 语言变量定义与赋值的过程。

```c
int a;
a = 10;
```



<span style="color:red;font-weight:bold;">（1）int a；</span> **在内存中分配了 4 个字节，变量的类型是 int，变量的名字是 a。**

<img src="Image/ddfsdsu8.JPG" alt="ddfsdsu8" style="zoom:76%;" />



<span style="color:red;font-weight:bold;">（2）a = 10；</span> **根据 变量名定位内存，并将 10 写入到内存单元，完成赋值操作。**

<img src="Image/dferereer.jpg" alt="dferereer" style="zoom:76%;" />



**变量的初始化：**变量可以在定义时对其赋值，称为初始化。变量没有赋值，其单元内容不可以使用，是内存中的随机数。变量在程序中可以被多次赋值。

```C
 
 #include<stdio.h>
 int main(){
 	int a=1,b=2,c=3,sum=0; //变量定义时对其完成复制操作。
 	float area, pi=3.1415,r=12;
 }
 
```





变量必须先定义，先赋值，后使用。 正确理解变量的概念

**变量名**：代表内存中的存储单元变量通过变量名标识，变量名和 内存中的存储单元相对应，通过变量名来存、取存储单元的内容。 

**变量的地址**：系统分配给变量的存储单元的起始地址。 

**变量的类型**：定义时指定，决定存储单元的大小和数据的存储方式。 

**变量的值**： 对应变量名存储单元存放的具体数值。







## 2 数据和类型

**变量的定义：**<span style="color:red;font-weight:bold;">类型说明符</span> <span style="color:green;font-weight:bold;">变量名1,变量名2，变量名3；</span>

在前不久，我们学习过变量的定义，其中，变量的定义的语法规则要求指定变量的数据类型，接下来我们来看看 C 语言中都有哪些数据类型可供我们选择。



### 2.1 整数类型

#### [1] char

------

char 字符在计算机的存储器中以<span style="color:red;font-weight:bold;"> 字符编码 </span>的形式保存，字符编码是一个数字，因此在计算机看来，A与数字 65 完全一样。

```C

char c = 'a'; // 注意字符常量 'a' 需要使用单引号包裹起来。

```

<img src="Image/544353.jpg" alt="img" style="zoom:68%;" />



**char** 类型用于储存字符（如，字母或标点符号），但是从技术层面看， **char** 是<span style="color:red;font-weight:bold;"> 整数类型</span>。因为 char 类型实际上储存的是整数而不是字符。计算机使用数字编码来处理字符，即用特定的整数表示特定的字符。美国最常用的编码是 ASCII 编码，在 ASCII 码中，整数 65 代表大写字母 A 。



```C

/*
 * 计算机 char 数据类型的长度 = 1 字节
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(char));
	return 0 ;
}

```

<img src="Image/image-20220327190719479.png" alt="image-20220327190719479" style="zoom:46%;" />

```c

#include<stdio.h>
int main(){
	char s = 'A';
	char si = 65;
	printf("%c\n",s); // 打印 'A'
	printf("%c",si);  // 猜一猜会打印什么呢？
	return 0;
}
// 在 C 语言中，用单引号括起来的单个字符被称为字符常量。编译器一发现 'A'，就会将其转换成相应的代码值。单引号必不可少。

```



##### 1）非打印字符

------

单引号只适用于字符、数字和标点符号，浏览ASCII表会发现，有些 ASCII 字符打印不出来。例如，一些代表行为的字符（如，退格、换行、终端响铃或蜂鸣）。C 语言提供了 3 种方法表示这些字符。 



接下来以蜂鸣字符为例。

第一种，蜂鸣字符的ASCII值是 7，因此可以这样写： 

```C

char beep = 7;

```

第二种，使用特殊的符号序列表示一些特殊的字符。这些符号序列叫作转义序列（escape sequence）。

> 接下来，请大家思考如何书写蜂鸣声这个字符？如何表示单引号（‘）这个字符

```C

char c = '\a';
char d = '\'';

```

<img src="Image/image-20220327203501684.png" alt="image-20220327203501684" style="zoom:80%;" />

第三种，使用 ASCII 码的特殊表示，<span style="color:red;font-weight:bold;"> 转义序列（\0oo和\xhh）</span>是 ASCII 码的特殊表示。如果要用八进制 ASCII 码表示一个字符，可以在编码值前面加一个反斜杠（\） 并用单引号括起来。

```c

char c = '\07';
char d = '\x41';
printf("%c",c);
printf("%c",d);

```



##### 2）打印字符 

------

printf() 函数用<span style="color:red;font-weight:bold;"> %c </span>指明待打印的字符。前面介绍过，一个字符变量实际上被储存为<span style="color:red;font-weight:bold;"> 1字节 </span>的整数值。因此，如果用<span style="color:red;font-weight:bold;"> %d </span>转换说明打印 char 类型<span style="color:red;font-weight:bold;"> 变量的值 </span>，打印的是一个整数。而<span style="color:red;font-weight:bold;"> %c </span>转换说明告诉printf( )打印该整数值对应的<span style="color:red;font-weight:bold;"> 字符 </span>。

```c

/* 
 *charcode.c-显示字符的代码编号 
 */ 
#include <stdio.h> 
int main() {
	char ch; 
	printf("Please enter a character.\n"); 
	scanf("%c", &ch); /* 用户输入字符 */ 
	printf("The code for %c is %d.\n", ch, ch); 
	return 0; 
}

```



#### [2] int

------

如果你要保存一个<span style="color:red;font-weight:bold;">  整数 </span>，通常可以使用 int。在现代操作系统中，int **一般**占用 4 个字节（Byte）的内存，共计 32 位（Bit）。如果不考虑正负数，当所有的位都为 1 时它的值最大，为 232-1 = 4,294,967,295 ≈ 43亿，这是一个很大的数，实际开发中很少用到，而诸如 1、99、12098 等较小的数使用频率反而较高。

```c

int i = 100000;

```



```c

/*
 * 计算机 int 数据类型的长度 = 4 字节 = 32bit
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(int));
	return 0 ;
}

```




使用 4 个字节保存较小的整数绰绰有余，会空闲出两三个字节来，这些字节就白白浪费掉了，不能再被其他数据使用。现在个人电脑的内存都比较大了，配置低的也有 2G，浪费一些内存不会带来明显的损失；而在C语言被发明的早期，或者在单片机和嵌入式系统中，内存都是非常稀缺的资源，所有的程序都在尽力节省内存。

反过来说，43 亿虽然已经很大，但要表示全球人口数量还是不够，必须要让整数占用更多的内存，才能表示更大的值，比如占用 6 个字节或者 8 个字节。



#### [3] short

------

但有时你想节省一点空间，毕竟如果只想保存一个几百、几千的数字，何必用int？可以用 short，short 通常只有 int 的一半大小。

```c

short s = 100;

```



```C

/*
 * 计算机 short 数据类型的长度 = 2 字节 = 16bit
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(short));
	return 0 ;
}

```





#### [4] long

------

但如果想保存一个很大的计数呢？long 数据类型就是为此而生的。在某些计算机中，long 的内存占用空间是 int 的两倍，所以可以保存更大的数字；



```C

long l = 100;

```



```c

/*
 * 计算机 long 数据类型的长度 = 4 字节 = 32bit
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(long));
	return 0 ;
}

```



#### [5] 整数类型汇总

下表列出了关于标准整数类型的存储大小和值范围的细节，注意，各种类型的存储大小与系统位数有关。

<img src="Image/image-20220327130104762.png" alt="image-20220327130104762" style="zoom: 60%;" />





### 2.2 小数 (浮点型) 类型

------

#### [1] float

float 是保存浮点数的基本数据类型。平时你会碰到很多浮点数，比如一杯香橙摩卡冰乐有多少毫升，就可以用 float 保存。

```C

float l = 100.1;

```



```c

/*
 * 计算机 float 数据类型的长度 = 4 字节 = 32bit
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(float));
	return 0 ;
}

```



#### [2] double

但如果想表示很精确的浮点数呢？如果想让计算结果精确到小数点以后很多位，可以使用 double。double 比 float 多占一倍空间，可以保存更大、更精确的数字。



```c

/*
 * 计算机 double 数据类型的长度 = 8 字节 = 64bit
 */
#include<stdio.h>
int main(){
	printf("%d",sizeof(double));
	return 0 ;
}


```



#### [3] 小数类型汇总

下表列出了关于标准浮点类型的存储大小、值范围和精度的细节。

<img src="Image/image-20220327130243564.png" alt="image-20220327130243564" style="zoom:66%;" />



### 2.3 按需索取

------

赋值时要保证值的类型与保存它的变量类型相匹配。不同数据类型的大小不同，千万别让值的大小超过变量。short 比 int 的空间小，int又比 long 小。

<img src="Image/image-20220326224425155.png" alt="image-20220326224425155" style="zoom:60%;" />

完全可以在 int 或 long 变量中保存 short 值。因为变量有足够的空间，你的代码将正确运行。

```c

short x = 15;
int y = x;
printf("y的值是%i\n", y);

```



但是反过来，比如你想在short变量中保存int值，就不行。

```c

int x = 100000;
short y = x;
print("y的值是%hi\n", y);

```





### 2.4 类型转换

------

你认为下面这段代码将显示什么？

```c

int x = 7;
int y = 2;
float z = x / y;
printf("z = %f\n", z);

// 答案是 3.000000。为什么是3.0000--？因为x和y都是整型，而两个整型相除，结果是一个舍入的整数，在这个例子中就是 3。

```



如果希望两个整数相除的结果是浮点数，应该先把整数保存到 float 变量中，但这样稍微有点麻烦，可以使用类型转换临时转换数值的类型：

```C

int x = 7;
int y = 2; 
float z = (float)x / (float)y; // (float) 会把int值转换为 float 值，计算时就可以把变量当成浮点数来用。
printf("z = %f\n", z);

```



事实上，如果编译器发现有整数在加、减、乘、除浮点数，会自动替你完成转换，因此可以减少代码中显式类型转换的次数：

<img src="Image/image-20220326225127081.png" alt="image-20220326225127081" style="zoom:70%;" />



### 2.5 练一练

------

不同平台上数据类型的大小不同，那怎么知道 int 或 double 占了多少字节？好在 C 标准库中有几个头文件提供了这些细节。下面这个程序将告诉你 int 与 float 的大小。

```C

#include <stdio.h>
#include <limits.h>
#include <float.h>
int main()
{
 	printf("The value of INT_MAX is %i\n", INT_MAX);
 	printf("The value of INT_MIN is %i\n", INT_MIN);
     printf("An int takes %i bytes\n", sizeof(int));
 	printf("The value of FLT_MAX is %f\n", FLT_MAX);
 	printf("The value of FLT_MIN is %.50f\n", FLT_MIN);
 	printf("A float takes %i bytes\n", sizeof(float));
 	return 0;
}

```





































































































# 附注



### 1 控制台中文乱码问题

**-finput-charset** 用来指定 C文件中的文字编码格式。

**-fexec-charset** 用来指定编译之后的可执行文件的文字编码格式；

默认情况下，gcc 编译器认为编译前后的文字编码格式都是 UTF-8。

<img src="Image/image-20220318200749670.png" alt="image-20220318200749670" style="zoom:50%;" />



### 2 Dev-C++ 错误提示查看

------

<img src="Image/234ewewe.gif" alt="234" style="zoom:80%;" />

